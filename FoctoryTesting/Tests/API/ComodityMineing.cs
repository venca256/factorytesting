﻿using NUnit.Framework;
using TestCore;
using TestCore.API;
using TestCore.API.DTOs;
using Newtonsoft.Json;
using System.Net;
using TestCore.API.DTOs.GET;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Tests.API
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class ComodityMineing : TestBase
    {
        [Test]
        [Category("HYBRID")]
        public void MineResource()
        {
            foreach (var user in Users.allUsers)
            {
                var originalCountOfStone = GetAmountOfCommodity("Stone");
                Driver.Navigate().Refresh();
                Helpers.Play(Driver, user);
                MineResource MineResource = new MineResource();
                MineResource.PlayerName = "Professor";
                MineResource.action = "MineResource";
                MineResource.coordinates = Helpers.GetPositionOfFirstStoneToMine(Driver);
                var json = JsonConvert.SerializeObject(MineResource);
                var response = Requests.SendJsonByPathAndMethodWithData(ServerApi + Commands.Turns, "POST", json);
                Assert.AreEqual(response.statusCode, HttpStatusCode.OK);
                Helpers.TickWait();
                var newMountOfCommodity=GetAmountOfCommodity("Stone");
                Assert.Greater(newMountOfCommodity,originalCountOfStone, "Main isn't working");
            }


            int GetAmountOfCommodity(string commodity)
            {
                var response = Requests.SendJsonByPathAndMethodWithData(ServerApi + Commands.Bank, "GET");
                var data = JsonConvert.DeserializeObject<IList<Class1>>(response.data);
                foreach (var commodit in data)
                {
                    if (commodit.Resource == commodity) { return commodit.Amount; };
                }
                return 0;
            };
        }


    }

}