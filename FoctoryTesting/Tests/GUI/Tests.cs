using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System.IO;
using System;
using System.Reflection;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using TestCore;
using ConfigEnvironment.Config;


namespace Tests.GUI
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)] //paralellize na urovni NUNITu
    public class Tests : TestBase
    {
        public object SettingsData { get; private set; }

        [SetUp]
        public void Setup()
        {

            //todo paralellizace na urovni TeamCity
            string user = Environment.GetEnvironmentVariable("user");
            if (user == null)
            {
                user = "Profesor";
            }
        }

        protected override void TearDown()
        {
            SceenshotIfFailed();
            base.TearDown();
        }

        [Test]
        [Category("GUI")]
        public void DefaultUsers()
        {
            IWebElement elem = Driver.FindElement(By.XPath(Selectors.playerName));
            SelectElement selectList = new SelectElement(elem);
            string defaultUser = selectList.SelectedOption.Text;
            Assert.AreEqual(defaultUser, Users.allUsers[8]);
            IList<IWebElement> options = selectList.Options;
            for (int i = 0; i < options.Count; i++)
            {
                Assert.AreEqual(options[i].Text, Users.allUsers[i], "Nespravny seznam useru");
            }
        }



        [Test]
        [Category("GUI")]
        public void AllUsersCanPlayAndMine()
        {
            foreach (var user in Users.allUsers)
            {
                Driver.Navigate().Refresh();
                Helpers.Play(Driver,user);
                var initialStoneEmanout = GetStoneAmount();
                GetFirstExtractableStone().Click();
                Driver.FindElement(By.XPath(Selectors.actionMineResource)).Click();
                Helpers.TickWait();
                Assert.AreNotEqual(initialStoneEmanout, GetStoneAmount());
            }
        }


        public IWebElement GetFirstExtractableStone()
        {
            IList<IWebElement> elements = Driver.FindElements(By.XPath("//table//img"));
            foreach (var element in elements)
            {
                var srcImage = element.GetAttribute("src");
                if (srcImage.Contains("img/Stone.png")) return element;
                //todo check that is possible to mine
            }
            return null;
        }



        public int GetStoneAmount()
        {
            return int.Parse(Driver.FindElement(By.XPath(Selectors.bankStoneAmount)).Text);
        }

    }
}