﻿using ConfigEnvironment.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using TestCore.API.DTOs;

namespace TestCore
{
    public class Helpers
    {
        public static void TakeScreenshot(IWebDriver driver, string testName)
        {
            var screenShot = ((ITakesScreenshot)driver).GetScreenshot();
            //var finalPath = Path.Combine(RootPath,path);
            var timestamp = DateTime.Now.ToString("dd_hh-mm-ss");
            var fileName = $"{testName}-{timestamp}.png";
            screenShot.SaveAsFile(TestBase.RootPath+ "/Results/Screenshots/"+fileName, ScreenshotImageFormat.Png);
        }

        public static void TickWait()
        {
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(TestConfig.SettingsData.tickTimeout));
        }

        public IWebElement GetFirstExtractableStone(IWebDriver driver)
        {
            IList<IWebElement> elements = driver.FindElements(By.XPath("//table//img"));
            foreach (var element in elements)
            {
                var srcImage = element.GetAttribute("src");
                if (srcImage.Contains("img/Stone.png")) return element;
                //todo check that is possible to mine
            }
            return null;
        }

        public static Coordinates GetPositionOfFirstStoneToMine(IWebDriver driver)
        {
            var countOfColumns = TestConfig.SettingsData.countOfColumns;
            var countOfRows = TestConfig.SettingsData.countOfRows;
            //todo check on initilaPage
            //int countOfRows = driver.FindElements(By.XPath("//table//tr")).Count;
            for (int i = 0; i < countOfRows; i++)
            {
                IList<IWebElement> elements = driver.FindElements(By.XPath("//table/tr//img"));
                int columnIndex = 0;
                int rowIndex = 0;
                foreach (var element in elements)
                {
                    var srcImage = element.GetAttribute("src");
                    if (srcImage.Contains("img/Stone.png")) return new Coordinates { X = rowIndex, Y = columnIndex };
                    if (columnIndex == countOfColumns-1) rowIndex++;
                    columnIndex++; 
                }
            }
            return null;
        }

        public static void Play(IWebDriver driver,string user) {
            SelectUser(driver, user);
            driver.FindElement(By.XPath(Selectors.playButton)).Click();
        }

        public static void SelectUser(IWebDriver driver, string name)
        {
            IWebElement elem = driver.FindElement(By.XPath(Selectors.playerName));
            SelectElement selectList = new SelectElement(elem);
            selectList.SelectByText(name);
        }
    }
}
