﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestCore.API.DTOs.GET
{
    public class Class1
    {
        [JsonProperty("Resource")]
        public string Resource { get; set; }
        [JsonProperty("Amount")]
        public int Amount { get; set; }
    }

}
