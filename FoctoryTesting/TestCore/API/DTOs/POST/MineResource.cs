﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCore.API.DTOs
{
        public class MineResource
        {
            public string PlayerName { get; set; }
            public string action { get; set; }
            public Coordinates coordinates { get; set; }
        }

        public class Coordinates
        {
            public int X { get; set; }
            public int Y { get; set; }
        }
}
