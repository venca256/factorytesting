﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace TestCore.API
{
    public class Requests
    {
        public class ResponseData
        {
            public HttpStatusCode statusCode;
            public string data;
            public CookieCollection cookie;
            public WebHeaderCollection headerCollection;
            public string EventId;
        }

        /// <param name="login"></param>
        /// <param name="path"></param>
        /// <param name="method"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        public static ResponseData SendJsonByPathAndMethodWithData(string path, string method, string json = null, string lastPublishedEventId = null)
        {
            ResponseData responseData = new ResponseData();

            var cc = new CookieContainer();
            var req = (HttpWebRequest)WebRequest.Create(path);
            HttpWebResponse res = null;


            Console.Write("");
            Console.Write("--------------------------------------------------------");
            Console.Write("REQUEST");
            Console.Write("API: " + path);
            Console.Write("JSON: " + json);
            Console.Write("");

            try
            {
                req.CookieContainer = cc;
                req.ContentType = "text/plain; charset=utf-8";
                req.Method = method;
                if (method == "POST")
                {
                    using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                    {
                        streamWriter.Write(json);
                        streamWriter.Flush();
                    }
                }
                res = (HttpWebResponse)req.GetResponse();
                var s = res.GetResponseStream();
                StreamReader sr = null;
                if (s != null) sr = new StreamReader(s, Encoding.UTF8);
                responseData.data = sr.ReadToEnd();
                responseData.cookie = res.Cookies;
                responseData.statusCode = res.StatusCode;
                responseData.headerCollection = res.Headers;
            }
            catch (WebException ex)
            {

                Console.WriteLine(ex);
                res = (HttpWebResponse)ex.Response;
                Stream resst = res.GetResponseStream();
                StreamReader sr = new StreamReader(resst);
                responseData.data = sr.ReadToEnd();
                responseData.statusCode = res.StatusCode;

            }
            Thread.Sleep(60); //60ms is protection over DDOS attack
            return responseData;
        }
    }
}
