﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace ConfigEnvironment.Config
{
    public static class TestConfig 
    {
        public static Settings SettingsData;
        const string RootNode = "//Settings";
        public static string RootPath => new Lazy<string>(GetRootPath).Value;


        static string GetRootPath()
        {
            return GetTestBaseDirectory();
        }

        static string GetTestBaseDirectory(string sourceFile = null)
        {
            // Need two call because this class is in subdirectory
            return Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(sourceFile)));
        }



        public static void LoadXmlSettings()
        {
            string xml = File.ReadAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\..\..\..\..\TestCore\Config\config.xml");
            SettingsData = xml.ParseXML<Settings>();
        }

        public static T ParseXML<T>(this string @this) where T : class
        {
            var reader = XmlReader.Create(@this.Trim().ToStream(), new XmlReaderSettings() { ConformanceLevel = ConformanceLevel.Document });
            return new XmlSerializer(typeof(T)).Deserialize(reader) as T;
        }

        public static Stream ToStream(this string @this)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(@this);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }


    }
    
}