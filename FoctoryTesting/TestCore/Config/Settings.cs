﻿
// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class Settings
{

    private string serverGuiField;

    private string serverApiField;

    private byte tickTimeoutField;

    private byte driverTimeoutField;

    private byte countOfRowsField;

    private byte countOfColumnsField;

    /// <remarks/>
    public string ServerGui
    {
        get
        {
            return this.serverGuiField;
        }
        set
        {
            this.serverGuiField = value;
        }
    }

    /// <remarks/>
    public string ServerApi
    {
        get
        {
            return this.serverApiField;
        }
        set
        {
            this.serverApiField = value;
        }
    }

    /// <remarks/>
    public byte tickTimeout
    {
        get
        {
            return this.tickTimeoutField;
        }
        set
        {
            this.tickTimeoutField = value;
        }
    }

    /// <remarks/>
    public byte driverTimeout
    {
        get
        {
            return this.driverTimeoutField;
        }
        set
        {
            this.driverTimeoutField = value;
        }
    }

    /// <remarks/>
    public byte countOfRows
    {
        get
        {
            return this.countOfRowsField;
        }
        set
        {
            this.countOfRowsField = value;
        }
    }

    /// <remarks/>
    public byte countOfColumns
    {
        get
        {
            return this.countOfColumnsField;
        }
        set
        {
            this.countOfColumnsField = value;
        }
    }
}

