﻿using ConfigEnvironment.Config;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace TestCore
{
  
    public abstract class TestBase
    {
     
        public static IWebDriver Driver;
        public static string RootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\..\..\..\..\";
        public static string ServerGui;
        public static string ServerApi;

        [OneTimeSetUp]
        protected virtual void BaseSetUp()
        {
            var tempStorage = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TempStorage");
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddUserProfilePreference("download.prompt_for_download", false);
            chromeOptions.AddUserProfilePreference("download.default_directory", tempStorage);
            chromeOptions.AddUserProfilePreference("bookmark_bar.show_on_all_tabs", false); //hide bookmark bar which ICT forced by adding the Quadient bookmarks folder
            chromeOptions.AddArgument("start-maximized");
            var chromeDriverPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Driver = new ChromeDriver(chromeDriverPath, chromeOptions);
            TestConfig.LoadXmlSettings();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(TestConfig.SettingsData.driverTimeout);
            ServerGui = TestConfig.SettingsData.ServerGui;
            ServerApi = TestConfig.SettingsData.ServerApi;
            Driver.Navigate().GoToUrl(ServerGui);
        }

        protected virtual void SceenshotIfFailed(IWebDriver driverForScreen = null, string driverName = "driver")
        {
            if (TestContext.CurrentContext.Result.FailCount <= 0) return;
            if (driverForScreen == null)
            {
                driverForScreen = Driver;
            }
            Helpers.TakeScreenshot(driverForScreen, TestContext.CurrentContext.Test.Name);
        }



        [SetUp]
        protected virtual void TestSetUp()
        {

        }

        [TearDown]
        protected virtual void TearDown()
        {
            try
            {
                Driver.Close();
                Driver.Quit();
            }
            catch
            {
                // Catch the error, if it is not possible to close the driver.
            }
        }
    }
}
